package clases;

public class SistemaSolar {
	private Planetas[] planetas;

	public SistemaSolar(int numPlanetas) {
		planetas = new Planetas[numPlanetas];
	}

	public void altaPlaneta(String estrella, String nombre, int masaPlaneta, int numeroDeSatelites) {
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] == null) {

				planetas[i] = new Planetas(estrella);
				planetas[i].setMasaPlaneta(masaPlaneta);
				planetas[i].setNombre(nombre);
				planetas[i].setNumeroDeSatelites(numeroDeSatelites);
				break;
			}
		}

	}

	public void BuscarPlaneta(String nombre) {
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				if (planetas[i].getNombre().equals(nombre)) {
					System.out.println(planetas[i].toString());
				}
			}
		}
	}

	public void eliminarPlaneta(String nombre) {
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				if (planetas[i].getNombre().equals(nombre)) {
					planetas[i] = null;
				}
			}
		}
	}

	public void listarGeneral() {
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				System.out.println(planetas[i].toString());
			}
		}
	}

	public void listarPlanetaPorEstrella(String estrella) {
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				if (planetas[i].getEstrella().equals(estrella)) {
					System.out.println(planetas[i].toString());
				}
			}
		}
	}

	public void cambiarNombrePlaneta(String nombre, String nombreNuevo) {
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				if (planetas[i].getNombre().equals(nombre)) {
					planetas[i].setNombre(nombreNuevo);
				}
			}
		}
	}

	public void mostrarPlanetasOrdenadosPorMasa() {
		Planetas[] nuevosPlanetas = new Planetas[planetas.length];
		nuevosPlanetas = planetas;
		Planetas aux;
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				for (int j = i + 1; j < planetas.length; j++) {
					if (planetas[j] != null) {
						if (nuevosPlanetas[j].getMasaPlaneta() > nuevosPlanetas[i].getMasaPlaneta()) {
							aux = nuevosPlanetas[i];
							nuevosPlanetas[i] = nuevosPlanetas[j];
							nuevosPlanetas[j] = aux;

						}
					}
				}

			}
		}
		for (int k = 0; k < nuevosPlanetas.length; k++) {
			if (nuevosPlanetas[k] != null) {
				System.out.println(nuevosPlanetas[k].toString());
			}
		}
	}

	public void mostrarPlanetasOrdenadosPorOrdenAlfabetico() {
		Planetas[] nuevos = new Planetas[planetas.length];
		nuevos = planetas;
		Planetas aux;
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				for (int j = i + 1; j < planetas.length; j++) {
					if (planetas[j] != null) {
						if (nuevos[j].getNombre().compareToIgnoreCase(nuevos[i].getNombre()) < 0) {
							aux = nuevos[i];
							nuevos[i] = nuevos[j];
							nuevos[j] = aux;

						}
					}
				}

			}
		}
		for (int k = 0; k < nuevos.length; k++) {
			if (nuevos[k] != null) {
				System.out.println(nuevos[k].toString());
			}
		}

	}

	public void buscarPlanetaPorRangoDeTamanno(int tamanno1, int tamanno2) {
		for (int i = 0; i < planetas.length; i++) {
			if (planetas[i] != null) {
				if (planetas[i].getMasaPlaneta() >= tamanno1 && planetas[i].getMasaPlaneta() <= tamanno2) {
					System.out.println(planetas[i].toString());
				}

			}
		}
	}

}
